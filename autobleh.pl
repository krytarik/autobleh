use strict;
use Irssi;
use LWP::Simple;

#
# autobleh is an Irssi helper script for
# IRC channel ops and network staff.
#
# This script is licensed under GPL version 3
#   <https://www.gnu.org/licenses/gpl.html>
#

our $VERSION = "2.1";
our %IRSSI   = (
  authors => "Juergen (sysdef) Heine, "
    . "Tom (tomaw) Wesley, "
    . "Nathan (nhandler) Handler, "
    . "Krytarik (krytarik) Raido. "
    . "Based on auto_bleh.pl by Don Armstrong",
  contact => "krytarik\@tuxgarage.com",
  url     => "https://bitbucket.org/krytarik/autobleh",
  name    => "autobleh",
  description => "Provides "
    . "/ab /aq /af /ak /ar /abk /abr /amb /amk /amr /ai /abl /aql /aml /ao /ams /ats",
  license => "GPL3",
  changed => "2020-02-16",
);

# CHANGELOG
# 2020-02-16 - krytarik - Add handling of CIDR notations
# 2019-11-06 - krytarik - Get maximum nick name length from server
# 2018-05-01 - krytarik - Improve and extend handling of mass actions
# 2017-11-14 - krytarik - Add option to list users matching a mask
# 2017-11-06 - krytarik - Improve handling of reverts
#                         on connection failures
# 2017-09-30 - krytarik - Improve user name handling
# 2017-09-23 - krytarik - Add handling of IPv6 addresses
# 2017-02-13 - krytarik - Improve and extend ban/quiet search
# 2016-10-27 - krytarik - Add option to ban/quiet by IP address
# 2016-10-26 - krytarik - Add option to arbitrarily combine actions
# 2016-10-25 - krytarik - Improve readability of help, add options
# 2016-10-22 - krytarik - Add option to ban/quiet by account
# 2016-10-22 - krytarik - Add option to print user info
# 2016-05-16 - krytarik - Add option to list bans/quiets
# 2016-03-21 - krytarik - Merge ban, quiet, forward, and timeout sections,
#                         enabling more options (deprecating '/at')
# 2016-03-20 - krytarik - Add option to ban/quiet by nick or user name
# 2016-02-21 - Unit193  - Ban/quiet Freenode webchat users by IP address
# 2016-02-21 - krytarik - Multiple fixes, add timeout setting,
#                         add option to specify forward channel manually
# 2015-12-12 - sysdef   - do not use $_
# 2015-12-12 - sysdef   - minor fixes, more PBP compatible format
# 2012-01-19 - sysdef   - renamed example config file;
#                         fixed comment char(;) in config ini file
# 2011-02-02 - nhandler - Allow using $nick and $channel in bleh_remove_message
# 2010-12-13 - nhandler - Remove extra space to fix kick command (Thanks rww)
# 2010-12-04 - nhandler - Change bleh_at_stay_opped from 10 to 300 seconds
# 2010-07-24 - nhandler - Don't hardcode the path to the irssi directory
# 2010-07-23 - nhandler - Add check for updates and bleh_updates setting
# 2010-07-23 - nhandler - Use new ban forward syntax
# 2010-07-23 - nhandler - Turn $DEBUG insto a setting, bleh_debug
# 2010-07-23 - nhandler - Update quiet/ban exceptions
# 2010-07-23 - nhandler - Fix TIMEOUT default to really be 10 minutes
# 2010-07-23 - nhandler - add "/help autobleh" command
# ????-??-?? - tomaw    - add irssi setting bleh_deop_after_action
# 2008-01-08 - sysdef   - add irssi setting bleh_remove_message
# 2008-01-08 - sysdef   - add command op
# 2008-01-08 - sysdef   - add command topicset
# 2008-01-08 - sysdef   - add command modeset
# 2008-01-22 - sysdef   - solve the "Unable to find nick: CHANSERV" error
# 2008-01-22 - sysdef   - add aliases /remove
# 2008-01-25 - sysdef   - add command af (auto forward / forward ban)

# read the config file
my $conffile = Irssi::get_irssi_dir() . "/autobleh.conf";
my ($conf_fh, $category, $config,
    $val, $key, $linecount);

if (open($conf_fh, "<", $conffile)) {
  Irssi::print("Loading autobleh config...");
}

foreach my $line (<$conf_fh>) {
  chomp $line;
  $linecount++;

  # skip comments and empty lines
  next if $line =~ /^\s*(;|$)/;

  # new category
  if ($line =~ /^\[([^ ]+)\]$/) {
    $category = $1;
    Irssi::print("Category: " . $category);
  }

  # get key/value pair
  elsif ($category and $line =~ /^([^ ]+)\s*=\s*([^ ]+)$/) {
    Irssi::print("Line $linecount: $category '$1' => '$2'");
    $config->{$category}{$1} = $2;
  }

  # crap line
  else {
    Irssi::print("PLEASE FIX YOUR CONFIG, LINE "
        . $linecount . ": " . $line);
  }
}
close $conf_fh;

my $DEBUG;
my $actions = {};
my $whois = {};
my $modes = {};
my $wholist = {};

my %defaults = (
  GET_OP       => 1,     # Should we try to get opped on action?
  DEOP         => 1,     # We want to deop after action.
  USE_CHANSERV => 1,     # Should we use ChanServ to get opped?
  EXPIRE       => 10,    # Do not try to do anything if the action
                         #   is more than N seconds old.
);

sub cmd_ah {
  print CLIENTCRAP <<HELP;

ao, op:              Ops a user, e.g. /ao bofh
ams, modeset:        %|Sets any mode you want, e.g. /ams -b *!*\@trollhost
ats, topicset:       %|Sets the topic, e.g. /ats No trolls allowed in here
ab, ban:             Bans a user, e.g. /ab sometroll
aq, quiet:           Quiets a user.
af, forward:         %|Sets a forward ban if defined in the config file, or if specified manually, e.g. /af sometroll [#fwdchannel]
aub, unban:          Unbans a user, e.g. /aub sometroll
auq, unquiet:        Unquiets a user.
auf, unforward:      %|Unsets a forward ban if defined in the config file, or if specified manually, e.g. /auf sometroll [#fwdchannel]
ak, kick:            Kicks a user, e.g. /ak sometroll [message]
ar, remove:          Removes a user.
abk, kickban:        Kickbans a user, e.g. /abk sometroll [message]
abr, removeban:      Bans and removes a user.
amb, massban:        %|Bans more than one user, e.g. /amb sometroll1 sometroll2
amq, massquiet:      Quiets more than one user.
amk, masskick:       %|Kicks more than one user, e.g. /amk sometroll1 sometroll2 [| message]
amr, massremove:     Removes more than one user.
ambk, masskickban:   %|Kickbans more than one user, e.g. /ambk sometroll1 sometroll2 [| message]
ambr, massremoveban: Bans and removes more than one user.
ai, userinfo:        Prints user info, e.g. /ai [sometroll]
abl, banlist:        Lists bans, e.g. /abl [sometroll]
aql, quietlist:      Lists quiets.
aml, matchlist:      Lists users matching a mask, e.g. /aml *!*\@host

Options:
  -n, -u             Action on nick or user name.
  -a, -i             Action on account or IP address.
  -t<N>              Revert action after N minutes.

Example 'autobleh.conf':
; comment
[forwardban]
#channel = ##fix_your_connection"
HELP
}

sub cmd_ao {
  my ($data, $server, $witem) = @_;
  do_bleh("op", $data, $server, $witem);
}

sub cmd_ams {
  my ($data, $server, $witem) = @_;
  do_bleh("modeset", $data, $server, $witem);
}

sub cmd_ats {
  my ($data, $server, $witem) = @_;
  do_bleh("topicset", $data, $server, $witem);
}

sub cmd_ab {
  my ($data, $server, $witem) = @_;
  do_bleh("ban", $data, $server, $witem);
}

sub cmd_aub {
  my ($data, $server, $witem) = @_;
  do_bleh("unban", $data, $server, $witem);
}

sub cmd_aq {
  my ($data, $server, $witem) = @_;
  do_bleh("quiet", $data, $server, $witem);
}

sub cmd_auq {
  my ($data, $server, $witem) = @_;
  do_bleh("unquiet", $data, $server, $witem);
}

sub cmd_af {
  my ($data, $server, $witem) = @_;
  do_bleh("forward", $data, $server, $witem);
}

sub cmd_auf {
  my ($data, $server, $witem) = @_;
  do_bleh("unforward", $data, $server, $witem);
}

sub cmd_ak {
  my ($data, $server, $witem) = @_;
  do_bleh("kick", $data, $server, $witem);
}

sub cmd_ar {
  my ($data, $server, $witem) = @_;
  do_bleh("remove", $data, $server, $witem);
}

sub cmd_abk {
  my ($data, $server, $witem) = @_;
  do_bleh("ban,kick", $data, $server, $witem);
}

sub cmd_abr {
  my ($data, $server, $witem) = @_;
  do_bleh("ban,remove", $data, $server, $witem);
}

sub cmd_afk {
  my ($data, $server, $witem) = @_;
  do_bleh("forward,kick", $data, $server, $witem);
}

sub cmd_afr {
  my ($data, $server, $witem) = @_;
  do_bleh("forward,remove", $data, $server, $witem);
}

sub cmd_akn {
  my ($data, $server, $witem) = @_;
  do_bleh("kick,notice", $data, $server, $witem);
}

sub cmd_abkn {
  my ($data, $server, $witem) = @_;
  do_bleh("ban,kick,notice", $data, $server, $witem);
}

sub cmd_arn {
  my ($data, $server, $witem) = @_;
  do_bleh("remove,notice", $data, $server, $witem);
}

sub cmd_abrn {
  my ($data, $server, $witem) = @_;
  do_bleh("ban,remove,notice", $data, $server, $witem);
}

sub cmd_ai {
  my ($data, $server, $witem) = @_;
  do_bleh("userinfo", $data, $server, $witem);
}

sub cmd_abl {
  my ($data, $server, $witem) = @_;
  do_bleh("banlist", $data, $server, $witem);
}

sub cmd_aql {
  my ($data, $server, $witem) = @_;
  do_bleh("quietlist", $data, $server, $witem);
}

sub cmd_aml {
  my ($data, $server, $witem) = @_;
  do_bleh("matchlist", $data, $server, $witem);
}

sub cmd_ac {
  my ($data, $server, $witem) = @_;
  my @data = split(' ', $data, 2);
  do_bleh($data[0], $data[1], $server, $witem);
}

sub cmd_amb {
  my ($data, $server, $witem) = @_;
  do_multi("ban", $data, $server, $witem);
}

sub cmd_amq {
  my ($data, $server, $witem) = @_;
  do_multi("quiet", $data, $server, $witem);
}

sub cmd_amk {
  my ($data, $server, $witem) = @_;
  do_multi("kick", $data, $server, $witem);
}

sub cmd_amr {
  my ($data, $server, $witem) = @_;
  do_multi("remove", $data, $server, $witem);
}

sub cmd_ambk {
  my ($data, $server, $witem) = @_;
  do_multi("ban,kick", $data, $server, $witem);
}

sub cmd_ambr {
  my ($data, $server, $witem) = @_;
  do_multi("ban,remove", $data, $server, $witem);
}

sub do_multi {
  my ($cmd, $data, $server, $witem) = @_;
  $data =~ /^\s*(-\S+)?\s*(.*?)\s*(\|\s*(.*?))?\s*$/;
  my ($opts, $nicks, $reason) = ($1, $2, $4);
  my @nicks = split(' ', $nicks);
  my ($nickcnt, $nickno, $multi) = (scalar(@nicks), 0, "first");
  foreach my $nick (@nicks) {
    $nickno++;
    if ($nickno == $nickcnt) {
      $multi = "last";
    } elsif ($nickno > 1) {
      $multi = "middle";
    }
    do_bleh($cmd, "$opts $nick $reason", $server, $witem, $multi);
  }
}

sub do_bleh {
  my ($cmd, $data, $server, $witem, $multi) = @_;

  # Reset on every run
  unless ($multi and $multi ne "first") {
    $actions = {};
    $whois = {};
    $modes = {};
    $wholist = {};
  }

  if (!$server or !$server->{connected}) {
    Irssi::print("Not connected to server.");
    return;
  }

  # Set variables from input data
  my ($channel, $nick, $fwdchan, $opts, $timeout, $reason);
  my ($username, $usernams, $hostname,
      $mask, $account, $realname, $ipaddr, $ipaddrs);
  my ($nickchk, $needop) = (1, 1);

  if ($cmd =~ /^(modeset|topicset)$/) {
    $nickchk = 0;
    $data =~ /^\s*([#&]\S+)?\s*(.*?)\s*$/;
    if ($2) {
      $reason = $2;
    } else {
      $needop = 0;
      $channel = $1;
    }
  }
  elsif ($cmd =~ /^(banlist|quietlist)$/) {
    $needop = 0;
    $data =~ /^\s*([#&]\S+)?\s*(\S+)?\s*/;
    $channel = $1;
    if ($2) {
      $nick = $2;
    } else {
      $nickchk = 0;
    }
  }
  elsif ($cmd eq "matchlist") {
    $needop = 0;
    $data =~ /^\s*([#&]\S+)?\s*(\S+)?\s*/;
    $channel = $1;
    if ($2) {
      $nick = $2;
    } else {
      Irssi::print("No target mask.");
      return;
    }
  }
  elsif ($cmd eq "userinfo") {
    $needop = 0;
    $data =~ /^\s*(\S+)\s*/;
    $nick = $1;
  }
  else {
    $data =~ /^\s*(-[a-z]+([0-9]+)?\S*)?\s*(\S+)?\s*([#&]\S+)?\s*(.*?)\s*$/;
    ($opts, $timeout, $nick, $fwdchan, $reason) = ($1, $2, $3, $4, $5);
    $timeout = Irssi::settings_get_int("bleh_revert_timeout")
      if not defined $timeout or $timeout eq q{};
    $reason = Irssi::settings_get_str("bleh_remove_message")
      if not defined $reason or $reason eq q{};
  }

  if ($cmd ne "userinfo") {
    if (!$channel) {
      if ($witem and $witem->{type} eq "CHANNEL") {
        $channel = $witem->{name};
      }
      else {
        Irssi::print("Not run in channel.");
        return;
      }
    }
  } else {
    if ($witem) {
      $channel = $witem->{name};
    }
    if (!$nick) {
      if ($witem and $witem->{type} eq "QUERY") {
        $nick = $channel;
      }
      else {
        Irssi::print("No target nick.");
        return;
      }
    }
  }

  if ($cmd =~ /(^|,)(un)?forward(,|$)/) {
    if ($fwdchan) {
      $config->{forwardban}{$witem->{name}} = $fwdchan;
    }
    if (!$config->{forwardban}{$witem->{name}}) {
      Irssi::print("Forward channel for "
          . $witem->{name}
          . " is not set. Please edit section "
          . "[forwardban] in '" . $conffile
          . "', or specify it manually.");
      return;
    }
  }

  if ($nickchk) {
    unless (nick_validate($server, $nick)) {
      my $nicko = $nick;
      $nick =~ s/^(.+?)(\$.*)?$/$1/;
      if ($nick =~ /^([^\$][^ ]*![^ ]+@[^ ]+|\$x:[^ ]+)$/) {
        $mask = $nicko;
        if ($nick =~ /^(\$x:)?([^ ]+)!([^ ]+)@([^ ]+?)(#([^ ]+))?$/) {
          ($nick, $username, $hostname, $realname) = ($2, $3, $4, $6);
          $usernams = get_usernams($username);
          ($ipaddr, $ipaddrs) = get_ipaddr($hostname);
        }
        $nickchk = 0;
      }
      elsif ($nick =~ /^\$a:[^ ]+$/) {
        $mask = $nicko;
        $account = substr($nick, 3);
        $nickchk = 0;
      }
      elsif ($nick =~ /^\$r:[^ ]+$/) {
        $mask = $nicko;
        $realname = substr($nick, 3);
        $nickchk = 0;
      }
      elsif ($nick =~ /^\$(j:[^ ]+|~a)$/) {
        $mask = $nicko;
        $nickchk = 0;
      }
      else {
        Irssi::print("Invalid target: '$nicko'");
        return;
      }
    }
    elsif ($cmd eq "matchlist") {
      Irssi::print("Invalid target: '$nick'");
      return;
    }
  }

  $reason =~ s/\$nick/$nick/g;
  $reason =~ s/\$channel/$witem->{name}/g;

  Irssi::print("Nick set to '$nick', "
      . "reason set to '$reason', from '$data'")
    if $DEBUG;

  if ($cmd =~ /(^|,)(kick|remove)(,|$)/
      and not $witem->nick_find($nick)) {
    Irssi::print("Unable to find nick: '$nick'");
    return;
  }

  my $action = {
    type      => $cmd,
    opts      => $opts,
    server    => $server,
    network   => $server->{chatnet},
    witem     => $witem,
    channel   => $channel,
    nick      => $nick,
    reason    => $reason,
    username  => $username,
    usernams  => $usernams,
    hostname  => $hostname,
    mask      => $mask,
    account   => $account,
    realname  => $realname,
    ipaddr    => $ipaddr,
    ipaddrs   => $ipaddrs,
    timeout   => $timeout,
    multi     => $multi,
    nickchk   => $nickchk,
    needop    => $needop,
    inserted  => time,
    deop      => 0,
  };

  if ($mask) {
    if (Irssi::settings_get_bool("bleh_info_on_action") or $cmd eq "userinfo") {
      $server->printformat($channel, MSGLEVEL_CLIENTCRAP,
        "bleh_user", $mask, $account, $realname);
    }
  }

  if ($nickchk) {
    $server->redirect_event("whois", 1, $nick, 0, "", {
      "event 311" => "redir bleh_whoisuser",
      "event 330" => "redir bleh_whoisaccount",
    # "event 312" => "redir bleh_whoisserver",
    # "event 313" => "redir bleh_whoisoperator",
    # "event 317" => "redir bleh_whoisidle",
    # "event 301" => "redir bleh_whoisaway",
    # "event 319" => "redir bleh_whoischannels",
      "event 307" => "redir bleh_whoisregnick",
    # "event 335" => "redir bleh_whoisbot",
    # "event 379" => "redir bleh_whoismodes",
    # "event 671" => "redir bleh_whoissecure",
    # "event 275" => "redir bleh_whoissecure",
    # "event 276" => "redir bleh_whoiscert",
    # "event 378" => "redir bleh_whoishost",
    # "event 338" => "redir bleh_whoisactually",
      "event 318" => "redir bleh_endofwhois",
      "event 401" => "redir bleh_nosuchnick",
      ""          => "event empty",
    });
    $whois->{$nick . "|" . $action->{inserted}} = $action;
    $server->send_raw("whois " . $nick);
  }
  else {
    schedule_action($action);
  }
}

sub schedule_action {
  my ($action) = @_;
  Irssi::print(i_want($action)) if $DEBUG;

  unless ($action->{needop} and not $action->{witem}{chanop}) {
    Irssi::print("Take action " . $action) if $DEBUG;
    take_action($action, $action->{server}, $action->{witem});
  }
  else {
    $actions->{$action->{nick} . "|" . $action->{inserted}} = $action;
    unless ($action->{multi} and $action->{multi} ne "last") {
      Irssi::print("Try to get op in " . $action->{channel}) if $DEBUG;
      get_op($action->{server}, $action->{channel}) if $defaults{GET_OP};
    }
  }
}

sub i_want {
  my ($action) = @_;
  return
      "I've wanted to "
    . $action->{type} . " "
    . $action->{nick} . " in "
    . $action->{channel} . " on "
    . $action->{network}
    . " since "
    . $action->{inserted};
}

sub get_op {
  my ($server, $channel) = @_;
  Irssi::print("CS op " . $channel) if $DEBUG;
  $server->command("quote CS op " . $channel) if $defaults{USE_CHANSERV};
}

sub take_action {
  my ($action, $server, $channel) = @_;

  Irssi::print(i_want($action)) if $DEBUG;

  if ($action->{type} =~ /(^|,)op(,|$)/) {
    if ($action->{nick} eq $action->{server}{nick}) {
      Irssi::print("Give op to myself.") if $DEBUG;
      $defaults{DEOP} = 0;
    }
    else {
      Irssi::print("Give op to " . $action->{nick}) if $DEBUG;
      $channel->command("quote MODE " . $action->{channel}
                       . " +o " . $action->{nick});
    }
  }

  if ($action->{type} =~ /(^|,)modeset(,|$)/) {
    Irssi::print("Set mode on " . $action->{channel}) if $DEBUG;

    # Set channel mode
    $server->command("quote MODE " . $action->{channel}
                     . " " . $action->{reason});
  }

  if ($action->{type} =~ /(^|,)topicset(,|$)/) {
    Irssi::print("Set topic on " . $action->{channel}) if $DEBUG;

    # Set topic
    if ($action->{reason}) {
      $server->command("quote TOPIC " . $action->{channel}
                       . " :" . $action->{reason});
    } else {
      $server->command("quote TOPIC " . $action->{channel});
    }
  }

  if ($action->{type} =~ /(^|,)(un)?(ban|quiet|forward)(,|$)/) {
    my ($actword, $actcmd, $actrev, $fwdchan, $fwdapp);

    if ($action->{type} =~ /(^|,)ban(,|$)/) {
      $actword = "Banning " . $action->{nick}
                 . " from " . $action->{channel};
      $actcmd = "quote MODE " . $action->{channel}
                . " +b ";
      $actrev = "unban";
    }
    elsif ($action->{type} =~ /(^|,)unban(,|$)/) {
      $actword = "Unbanning " . $action->{nick}
                 . " from " . $action->{channel};
      $actcmd = "quote MODE " . $action->{channel}
                . " -b ";
      $actrev = "ban";
    }
    elsif ($action->{type} =~ /(^|,)quiet(,|$)/) {
      $actword = "Quieting " . $action->{nick}
                 . " on " . $action->{channel};
      $actcmd = "quote MODE " . $action->{channel}
                . " +q ";
      $actrev = "unquiet";
    }
    elsif ($action->{type} =~ /(^|,)unquiet(,|$)/) {
      $actword = "Unquieting " . $action->{nick}
                 . " on " . $action->{channel};
      $actcmd = "quote MODE " . $action->{channel}
                . " -q ";
      $actrev = "quiet";
    }
    elsif ($action->{type} =~ /(^|,)forward(,|$)/) {
      $fwdchan = $config->{forwardban}{$action->{channel}};
      $fwdapp = "\$" . $fwdchan;
      $actword = "Forwarding " . $action->{nick}
                 . " from " . $action->{channel}
                 . " to " . $fwdchan;
      $actcmd = "quote MODE " . $action->{channel}
                . " +b ";
      $actrev = "unforward";
    }
    elsif ($action->{type} =~ /(^|,)unforward(,|$)/) {
      $fwdchan = $config->{forwardban}{$action->{channel}};
      $fwdapp = "\$" . $fwdchan;
      $actword = "Unforwarding " . $action->{nick}
                 . " from " . $action->{channel}
                 . " to " . $fwdchan;
      $actcmd = "quote MODE " . $action->{channel}
                . " -b ";
      $actrev = "forward";
    }

    if ($action->{mask} && $action->{opts} !~ /[nui]/)
    {
      Irssi::print($actword
          . " with mask "
          . $action->{mask})
        if $DEBUG;

      # action on mask
      $channel->command($actcmd
          . $action->{mask}
          . $fwdapp)
        if $action->{mask} ne q{};
    }
    elsif ($action->{opts} =~ /a/ && $action->{account})
    {
      Irssi::print($actword
          . " with account "
          . $action->{account})
        if $DEBUG;

      # action on account
      $channel->command($actcmd
          . "\$a:" . $action->{account}
          . $fwdapp)
        if $action->{account} ne q{};
    }
    elsif ($action->{opts} =~ /n/)
    {
      Irssi::print($actword
          . " with nickname "
          . $action->{nick})
        if $DEBUG;

      # action on nickname
      $channel->command($actcmd
          . $action->{nick} . "!*\@*"
          . $fwdapp)
        if $action->{nick} ne q{};
    }
    elsif ($action->{opts} !~ /u/
      && (($action->{opts} =~ /i/ && $action->{ipaddrs})
        || $action->{hostname} =~ m@^gateway/web/freenode/.+@))
    {
      Irssi::print($actword
          . " with IP address "
          . $action->{ipaddrs})
        if $DEBUG;

      # action on IP address
      $channel->command($actcmd
          . "*!*\@" . $action->{ipaddrs}
          . $fwdapp)
        if $action->{ipaddrs} ne q{};
    }
    elsif ($action->{opts} =~ /u/
      || $action->{hostname} =~ m@^(gateway/shell/.+/)x-.+@
      || $action->{hostname} =~ m@^(gateway/web/).+/.+@
      || $action->{hostname} =~ m@^((conference|nat)/.+/)x-.+@)
    {
      Irssi::print($actword
          . " with username "
          . $action->{usernams})
        if $DEBUG;

      # action on username
      $channel->command($actcmd
          . "*!" . $action->{usernams}
          . "\@" . $1 . "*"
          . $fwdapp)
        if $action->{usernams} ne q{};
    }
    else {
      Irssi::print($actword
          . " with hostname "
          . $action->{hostname})
        if $DEBUG;

      # action on hostname
      $channel->command($actcmd
          . "*!*\@" . $action->{hostname}
          . $fwdapp)
        if $action->{hostname} ne q{};
    }

    if ($actrev eq "unforward"
      && nick_validate($server, $action->{nick}))
    {
      # notice user
      $channel->command("NOTICE "
          . $action->{nick}
          . " You got a forward ban from "
          . $action->{channel} . " to "
          . $fwdchan);
    }

    if ($action->{opts} =~ /t/ && $action->{timeout})
    {
    Irssi::print("Scheduling " . $actrev . " in "
        . $action->{timeout} . " minutes.")
      if $DEBUG;

      # don't deop on a short time
      if ($defaults{DEOP} and $action->{deop}) {
        my $revert_stay_opped = Irssi::settings_get_int("bleh_revert_stay_opped");
        if ($action->{timeout} <= $revert_stay_opped) {
          Irssi::print("I'll stay opped until " . $actrev . " because "
              . $action->{timeout} . " <= " . $revert_stay_opped)
            if $DEBUG;
          $defaults{DEOP} = 0;
        }
        else {
          Irssi::print("I'll NOT stay opped until " . $actrev . " because "
              . $action->{timeout} . " > " . $revert_stay_opped)
            if $DEBUG;
        }
      }
      $action->{type} = $actrev;
      $action->{opts} =~ s/t[0-9]*//g;
      Irssi::timeout_add_once($action->{timeout} * 60000, "revert_action", $action);
    }
  }

  if ($action->{type} =~ /(^|,)kick(,|$)/) {
    Irssi::print("Kicking " . $action->{nick} . " from " . $action->{channel})
      if $DEBUG;
    $channel->command("quote KICK "
        . $action->{channel} . " "
        . $action->{nick} . " :"
        . $action->{reason});
  }

  if ($action->{type} =~ /(^|,)remove(,|$)/) {
    Irssi::print("Removing " . $action->{nick} . " from " . $action->{channel})
      if $DEBUG;
    $channel->command("quote REMOVE "
        . $action->{channel} . " "
        . $action->{nick} . " :"
        . $action->{reason});
  }

  if ($action->{type} =~ /(^|,)notice(,|$)/) {
    Irssi::print("Noticing " . $action->{nick} . " with '$action->{reason}'")
      if $DEBUG;
    $channel->command("NOTICE " . $action->{nick} . " " . $action->{reason});
  }

  if ($action->{type} =~ /(^|,)banlist(,|$)/) {
    $modes->{$action->{channel}}{ban} = [];
    Irssi::print("Get bans on " . $action->{channel}) if $DEBUG;
    $server->redirect_event("mode b", 1, $action->{channel}, 0, "", {
      "event 367" => "redir bleh_banlist",
      "event 368" => "redir bleh_endofbanlist",
      ""          => "event empty",
    });
    $actions->{$action->{nick} . "|" . $action->{inserted}} = $action;
    $server->send_raw("mode " . $action->{channel} . " +b");
  }

  if ($action->{type} =~ /(^|,)quietlist(,|$)/) {
    $modes->{$action->{channel}}{quiet} = [];
    Irssi::print("Get quiets on " . $action->{channel}) if $DEBUG;
    $server->redirect_event("mode q", 1, $action->{channel}, 0, "", {
      "event 728" => "redir bleh_quietlist",
      "event 729" => "redir bleh_endofquietlist",
      "event 344" => "redir bleh_quietlist",
      "event 345" => "redir bleh_endofquietlist",
      ""          => "event empty",
    });
    $actions->{$action->{nick} . "|" . $action->{inserted}} = $action;
    $server->send_raw("mode " . $action->{channel} . " +q");
  }

  if ($action->{type} =~ /(^|,)matchlist(,|$)/) {
    $server->redirect_event("who", 1, $action->{channel}, 0, "", {
      "event 352" => "redir bleh_whoreply",
      "event 354" => "redir bleh_whospcrpl",
      "event 315" => "redir bleh_endofwho",
      ""          => "event empty"
    });
    $server->send_raw("who " . $action->{channel} . " %cnuhar");
    $actions->{$action->{channel} . "|" . $action->{inserted}} = $action;
  }
}

sub deop_us {
  my ($channel) = @_;
  Irssi::print(
    "MODE " . $channel->{name} . " -o " . $channel->{ownnick}{nick})
    if $DEBUG;
  $channel->command("quote MODE " . $channel->{name}
                    . " -o " . $channel->{ownnick}{nick});
}

sub sig_mode_change {
  # Are there any actions to process?
  # See if we got opped.
  return unless keys %{$actions};
  my ($channel, $nick, $set_by, $mode, $type) = @_;

  if ($channel->{server}{nick} eq $nick->{nick}
      and ($mode eq "@" and $type eq "+"))
  {
    Irssi::print("We've been opped in "
        . $channel->{name}) if $DEBUG;
    foreach my $action (keys %{$actions}) {

      if ($channel->{server}{chatnet} eq $actions->{$action}{network}
          and $channel->{name} eq $actions->{$action}{channel})
      {
        $actions->{$action}{deop} = 1;

        # See if this action is too old
        unless ($actions->{$action}{inserted} <= time - $defaults{EXPIRE})
        {
          take_action($actions->{$action}, $channel->{server}, $channel);
        }
        else {
          Irssi::print("Expiring action "
              . $actions->{$action}
              . " because of timeout.")
            if $DEBUG;
        }

        if ($actions->{$action}{multi}) {
          if (scalar(keys(%{$actions})) == 1) {
            $actions->{$action}{multi} = "last";
          } else {
            $actions->{$action}{multi} = "yes";
          }
        }

        if (Irssi::settings_get_bool("bleh_deop_after_action")
            and $defaults{DEOP} and $actions->{$action}{deop}
            and not ($actions->{$action}{multi}
            and $actions->{$action}{multi} ne "last"))
        {
            deop_us($channel);
            $actions->{$action}{deop} = 0;
        }
        $defaults{DEOP} = 1;
        delete $actions->{$action};
      }
    }
  } else {
    Irssi::print("Fooey. Not opped.") if $DEBUG;
  }
}

sub revert_action {
  my ($action) = @_;
  my $server = Irssi::server_find_chatnet($action->{network});

  if ($server && $server->{connected}
      && (my $witem = $server->channel_find($action->{channel}))) {
    $action->{server} = $server;
    $action->{witem} = $witem;
  }
  else {
    Irssi::timeout_add_once(Irssi::settings_get_int("bleh_revert_reschedule")
        * 60000, "revert_action", $action);
    return;
  }

  $action->{inserted} = time;
  $actions->{$action} = $action;

  if ($action->{witem}{chanop}) {
    Irssi::print("Take action " . $action) if $DEBUG;
    take_action($action, $action->{server}, $action->{witem});
    deop_us($action->{witem}) if $action->{deop};
    delete $actions->{$action};
  }
  else {
    unless ($action->{multi} and $action->{multi} ne "last") {
      Irssi::print("Try to get op in " . $action->{channel}) if $DEBUG;
      get_op($action->{server}, $action->{channel}) if $defaults{GET_OP};
    }
  }
}

sub print_modes {
  my ($server, $channel, $type) = @_;

  foreach my $action (keys %{$actions}) {
    if ($actions->{$action}{type} =~ /^(banlist|quietlist)$/
        and $actions->{$action}{network} eq $server->{chatnet}
        and $actions->{$action}{channel} eq $channel) {

      my $modesfnd = 0;
      if (!$actions->{$action}{nick}) {
        $server->printformat($channel, MSGLEVEL_CLIENTCRAP,
          "bleh_channel", $channel);
      }

      foreach my $mode (@{$modes->{$channel}{$type}}) {
        if (!$actions->{$action}{nick} or modematch(@$mode[0], $actions->{$action})) {
          $modesfnd = 1;
          $server->printformat($channel, MSGLEVEL_CLIENTCRAP, "bleh_bans",
            ucfirst($type), @$mode[0], @$mode[1], substr(@$mode[2], 4));
        }
      }

      if (!$modesfnd) {
        if (!$actions->{$action}{nick}) {
          $server->printformat($channel, MSGLEVEL_CLIENTCRAP,
            "bleh_nobans", $type, "channel");
        } else {
          $server->printformat($channel, MSGLEVEL_CLIENTCRAP,
            "bleh_nobans", $type, "user");
        }
      }
      delete $actions->{$action};
    }
  }
}

sub print_matches {
  my ($server, $channel) = @_;

  foreach my $action (keys %{$actions}) {
    if ($actions->{$action}{type} eq "matchlist"
        and $actions->{$action}{network} eq $server->{chatnet}
        and $actions->{$action}{channel} eq $channel) {

      my (@matches, $nicks);
      foreach my $nick (keys %{$wholist->{$channel}}) {
        if (modematch($actions->{$action}{mask}, $wholist->{$channel}{$nick})) {
          push(@matches, $nick);
        }
      }

      if (@matches) {
        my $mtchcnt = scalar(@matches);
        if ($mtchcnt <= 12) {
          $nicks = join(', ', sort(@matches));
        } else {
          $nicks = join(', ', (sort(@matches))[0..11]) . ", ...";
        }
        $server->printformat($channel, MSGLEVEL_CLIENTCRAP, "bleh_matches",
          $mtchcnt, $mtchcnt > 1 ? "s" : "", $nicks);
      } else {
        $server->printformat($channel, MSGLEVEL_CLIENTCRAP, "bleh_nomatches");
      }
      delete $actions->{$action};
    }
  }
}

sub modematch {
  my ($match, $action) = @_;
  $match =~ s/^(.+?)(\$.*)?$/$1/;
  if ($match =~ /^[^\$][^ ]*![^ ]+@(([0-9]{1,3}\.){3}[0-9]{1,3}|([0-9a-f]{0,4}:)+([0-9a-f]{1,4})?)\/[0-9]+$/i) {
    $match =~ s/@.*$//;
    $match = quotemeta($match);
    $match =~ s/\\\*/.*/g;
    $match =~ s/\\\?/./g;
    $match =~ s/!\\\~/!\\~?/;
    return sprintf("%s!%s", $action->{nick}, $action->{username}) =~ /^$match$/i;
  }
  elsif ($match =~ /^[^\$][^ ]*![^ ]+@[^ ]+$/) {
    $match = quotemeta($match);
    $match =~ s/\\\*/.*/g;
    $match =~ s/\\\?/./g;
    $match =~ s/!\\\~/!\\~?/;
    my $result = sprintf("%s!%s@%s", $action->{nick}, $action->{username},
      $action->{hostname}) =~ /^$match$/i;
    if (!$result && $action->{ipaddr}) {
      $result = sprintf("%s!%s@%s", $action->{nick}, $action->{username},
        $action->{ipaddr}) =~ /^$match$/i;
    }
    return $result;
  }
  elsif ($match =~ /^\$a:[^ ]+$/) {
    if ($action->{account}) {
      $match = substr($match, 3);
      $match = quotemeta($match);
      $match =~ s/\\\*/.*/g;
      $match =~ s/\\\?/./g;
      return $action->{account} =~ /^$match$/i;
    }
  }
  elsif ($match =~ /^\$r:[^ ]+$/) {
    if ($action->{realname}) {
      $match = substr($match, 3);
      $match = quotemeta($match);
      $match =~ s/\\\*/.*/g;
      $match =~ s/\\\?/./g;
      return $action->{realname} =~ /^$match$/i;
    }
  }
  elsif ($match =~ /^\$x:[^ ]+$/) {
    $match = substr($match, 3);
    $match = quotemeta($match);
    $match =~ s/\\\*/.*/g;
    $match =~ s/\\\?/./g;
    $match =~ s/!\\\~/!\\~?/;
    return sprintf("%s!%s@%s#%s", $action->{nick}, $action->{username},
      $action->{hostname}, $action->{realname}) =~ /^$match$/i;
  }
  elsif ($match =~ /^\$j:[^ ]+$/) {
    return 1;
  }
  elsif ($match =~ /^\$~a$/) {
    if (!$action->{account}) {
      return 1;
    }
  }
}

sub nick_validate {
  my ($server, $nick) = @_;
  my $nicklen = $server->isupport('NICKLEN');
  return $nick =~ /^[a-zA-Z_^`|\\[\]{}][-a-zA-Z0-9_^`|\\[\]{}]{0,@{[$nicklen - 1]}}$/;
}

sub get_usernams {
  my ($username) = @_;
  if ($username =~ /^\~([^ ]+)$/) {
    return '*' . $1;
  }
  elsif (length($username) <= 9) {
    return '*' . $username;
  }
  else {
    return $username;
  }
}

sub get_ipaddr {
  my ($hostname) = @_;
  if ($hostname =~ /(([0-9]{1,3}[.-]){3}[0-9]{1,3})/) {
    my $ipaddr = $1 =~ s/(^|[.-])0+([^.-]+)/$1$2/gr;
    return ($ipaddr =~ s/-/./gr) x 2;
  }
  elsif ($hostname =~ /^([0-9a-f]{0,4}:)+([0-9a-f]{1,4})?$/i) {
    return ($hostname, shorten_ipv6($hostname));
  }
  elsif ($hostname =~ /([0-9a-f]{32})/i) {
    my $ipaddr = join(':', lc($1) =~ /(....)/g) =~ s/(^|:)0+([^:]+)/$1$2/gr;
    return ($ipaddr =~ s/(^|:)(0(:|$)){2,}/::/r, shorten_ipv6($ipaddr));
  }
}

sub shorten_ipv6 {
  my ($ipaddr) = @_;
  if (my @ipsegs = $ipaddr =~ /^(.*)::(.*)$/) {
    $ipaddr = join(':', grep {/\S/} ($ipsegs[0],
      ("0") x (8 - scalar(() = $ipaddr =~ /([^:]+)/g)), $ipsegs[1]));
  }
  $ipaddr =~ s/(:[^:]{1,4}){4}$/:*/;
  return $ipaddr =~ s/(^|:)(0(:|$)){2,}/::/r;
}

sub banlist {
  my ($server, $data) = @_;
  my @data = split(' ', $data);
  push(@{$modes->{$data[1]}{ban}}, [$data[2], $data[3], scalar(localtime($data[4]))]);
}

sub quietlist {
  my ($server, $data) = @_;
  my @data = split(' ', $data);
  push(@{$modes->{$data[1]}{quiet}}, [$data[-3], $data[-2], scalar(localtime($data[-1]))]);
}

sub endofbanlist {
  my ($server, $data) = @_;
  my @data = split(' ', $data);
  print_modes($server, $data[1], "ban");
}

sub endofquietlist {
  my ($server, $data) = @_;
  my @data = split(' ', $data);
  print_modes($server, $data[1], "quiet");
}

sub whoreply {
  my ($server, $data) = @_;
  my @data = split(' ', $data, 9);
  $wholist->{$data[1]}{$data[5]} = { nick => $data[5], username => $data[2], hostname => $data[3],
    ipaddr => (get_ipaddr($data[3]))[0], account => "", realname => $data[8] };
}

sub whospcrpl {
  my ($server, $data) = @_;
  my @data = split(' ', $data, 7);
  $wholist->{$data[1]}{$data[4]} = { nick => $data[4], username => $data[2], hostname => $data[3],
    ipaddr => (get_ipaddr($data[3]))[0], account => $data[5], realname => $data[6] };
}

sub endofwho {
  my ($server, $data) = @_;
  my @data = split(' ', $data);
  print_matches($server, $data[1]);
}

sub whoisuser {
  my ($server, $data) = @_;
  my (undef, $nick, $username, $hostname, undef, $realname) = split(' ', $data, 6);
  foreach my $action (keys %{$whois}) {
    if (($whois->{$action}{nick} =~ /^\Q$nick\E$/i
        and $whois->{$action}{network} eq $server->{chatnet})
        and $whois->{$action}{nickchk}) {
      $whois->{$action}{nick} = $nick;
      $whois->{$action}{username} = $username;
      $whois->{$action}{usernams} = get_usernams($username);
      $whois->{$action}{hostname} = $hostname;
      $realname =~ s/^://;
      $whois->{$action}{realname} = $realname;
      ($whois->{$action}{ipaddr}, $whois->{$action}{ipaddrs}) = get_ipaddr($hostname);
      $whois->{$action}{nickchk} = 0;
    }
  }
}

sub whoisaccount {
  my ($server, $data) = @_;
  my (undef, $nick, $account) = split(' ', $data);
  foreach my $action (keys %{$whois}) {
    if (($whois->{$action}{nick} =~ /^\Q$nick\E$/i
        and $whois->{$action}{network} eq $server->{chatnet})
        and not $whois->{$action}{account}) {
      $whois->{$action}{account} = $account;
    }
  }
}

sub whoisregnick {
  my ($server, $data) = @_;
  my (undef, $nick) = split(' ', $data);
  foreach my $action (keys %{$whois}) {
    if ($whois->{$action}{nick} =~ /^\Q$nick\E$/i
        and $whois->{$action}{network} eq $server->{chatnet}) {
      $whois->{$action}{account} = $nick;
    }
  }
}

sub endofwhois {
  my ($server) = @_;
  foreach my $action (keys %{$whois}) {
    if ($whois->{$action}{network} eq $server->{chatnet}
        and not $whois->{$action}{nickchk}) {
      if (Irssi::settings_get_bool("bleh_info_on_action")
          or $whois->{$action}{type} eq "userinfo") {
        $server->printformat($whois->{$action}{channel},
          MSGLEVEL_CLIENTCRAP, "bleh_user", sprintf("%s!%s@%s",
          $whois->{$action}{nick}, $whois->{$action}{username},
          $whois->{$action}{hostname}), $whois->{$action}{account},
          $whois->{$action}{realname});
      }
      if ($whois->{$action}{multi}) {
        if (scalar(keys(%{$whois})) == 1) {
          $whois->{$action}{multi} = "last";
        } else {
          $whois->{$action}{multi} = "yes";
        }
      }
      schedule_action($whois->{$action});
      delete $whois->{$action};
    }
  }
}

sub nosuchnick {
  my ($server, $data) = @_;
  my (undef, $nick) = split(' ', $data);
  $server->redirect_event("whowas", 1, $nick, 0, "", {
    "event 314" => "redir bleh_whoisuser",
    "event 330" => "redir bleh_whoisaccount",
    "event 369" => "redir bleh_endofwhois",
    "event 406" => "redir bleh_wasnosuchnick",
    ""          => "event empty",
  });
  $server->send_raw("whowas " . $nick);
}

sub wasnosuchnick {
  my ($server, $data) = @_;
  my (undef, $nick) = split(' ', $data);
  foreach my $action (keys %{$whois}) {
    if ($whois->{$action}{nick} =~ /^\Q$nick\E$/i
        and $whois->{$action}{network} eq $server->{chatnet}) {
      Irssi::print("Unable to find nick: " . $nick);
      delete $whois->{$action};
    }
  }
}

sub check_updates {
  return unless Irssi::settings_get_bool("bleh_updates");
  my $latscr = get($IRSSI{url} . "/raw/HEAD/autobleh.pl");
  $latscr =~ /\nour \$VERSION = "([0-9.]+)";\n/;
  my $latest = $1;
  if (defined $latest) {
    my $current = $VERSION;
    if ($latest > $current) {
      Irssi::print("A new version of autobleh ("
          . $latest . ") is available at " . $IRSSI{url});
      Irssi::print("autobleh " . $current . " is currently installed.");
    }
    else {
      Irssi::print("autobleh (" . $current . ") is up to date.");
    }
  } else {
    Irssi::print("Failed to check for updates to autobleh.");
  }
}

Irssi::Irc::Server::redirect_register("mode q", 0, 0,
  { "event 728" => 1, "event 344" => 1 },
  { "event 729" => 1, "event 345" => 1,
    "event 403" => 1, "event 442" => 1,
    "event 479" => 1 }, {}
);

Irssi::command_bind({
  ao   => "cmd_ao",   #op               => "cmd_ao",
  ams  => "cmd_ams",  modeset          => "cmd_ams",
  ats  => "cmd_ats",  topicset         => "cmd_ats",
  ab   => "cmd_ab",   #ban              => "cmd_ab",
  aub  => "cmd_aub",  #unban            => "cmd_aub",
  aq   => "cmd_aq",   quiet            => "cmd_aq",
  auq  => "cmd_auq",  unquiet          => "cmd_auq",
  af   => "cmd_af",   forward          => "cmd_af",
  auf  => "cmd_auf",  unforward        => "cmd_auf",
  ak   => "cmd_ak",   #kick             => "cmd_ak",
  ar   => "cmd_ar",   #remove           => "cmd_ar",
  abk  => "cmd_abk",  kickban          => "cmd_abk",
  abr  => "cmd_abr",  removeban        => "cmd_abr",
  afk  => "cmd_afk",  forwardkick      => "cmd_afk",
  afr  => "cmd_afr",  forwardremove    => "cmd_afr",
  akn  => "cmd_akn",  kick_notice      => "cmd_akn",
  arn  => "cmd_arn",  remove_notice    => "cmd_arn",
  abkn => "cmd_abkn", kickban_notice   => "cmd_abkn",
  abrn => "cmd_abrn", removeban_notice => "cmd_abrn",
  amb  => "cmd_amb",  massban          => "cmd_amb",
  amq  => "cmd_amq",  massquiet        => "cmd_amq",
  amk  => "cmd_amk",  masskick         => "cmd_amk",
  amr  => "cmd_amr",  massremove       => "cmd_amr",
  ambk => "cmd_ambk", masskickban      => "cmd_ambk",
  ambr => "cmd_ambr", massremoveban    => "cmd_ambr",
  ai   => "cmd_ai",   userinfo         => "cmd_ai",
  abl  => "cmd_abl",  banlist          => "cmd_abl",
  aql  => "cmd_aql",  quietlist        => "cmd_aql",
  aml  => "cmd_aml",  matchlist        => "cmd_aml",
  ac   => "cmd_ac",   bleh_comb        => "cmd_ac",
  ah   => "cmd_ah",   bleh_help        => "cmd_ah",
}, "autobleh");

Irssi::theme_register([
  bleh_user      => '{hilight $0} [a: $1, r: $2]',
  bleh_channel   => 'Channel: {hilight $0}',
  bleh_bans      => '{hilight $0}: {ban $1} [by {hilight $2}, on $3]',
  bleh_nobans    => '{hilight No $0s for this $1.}',
  bleh_matches   => '{hilight Matches $0 user$1}: $2',
  bleh_nomatches => '{hilight No matches for this mask.}',
]);

Irssi::signal_add_last("nick mode changed", "sig_mode_change");

Irssi::signal_add({
  "redir bleh_whoisuser"     => "whoisuser",
  "redir bleh_whoisaccount"  => "whoisaccount",
  "redir bleh_whoisregnick"  => "whoisregnick",
  "redir bleh_endofwhois"    => "endofwhois",
  "redir bleh_nosuchnick"    => "nosuchnick",
  "redir bleh_wasnosuchnick" => "wasnosuchnick",
});

Irssi::signal_add({
  "redir bleh_whoreply"  => "whoreply",
  "redir bleh_whospcrpl" => "whospcrpl",
  "redir bleh_endofwho"  => "endofwho",
});

Irssi::signal_add({
  "redir bleh_banlist"        => "banlist",
  "redir bleh_endofbanlist"   => "endofbanlist",
  "redir bleh_quietlist"      => "quietlist",
  "redir bleh_endofquietlist" => "endofquietlist",
});

Irssi::settings_add_bool("autobleh", "bleh_info_on_action", 1);
Irssi::settings_add_bool("autobleh", "bleh_deop_after_action", 1);
Irssi::settings_add_str("autobleh", "bleh_remove_message", "Goodbye");
Irssi::settings_add_int("autobleh", "bleh_revert_stay_opped", 5);   # in minutes
Irssi::settings_add_int("autobleh", "bleh_revert_timeout", 10);   # in minutes
Irssi::settings_add_int("autobleh", "bleh_revert_reschedule", 10);   # in minutes

Irssi::settings_add_bool("autobleh", "bleh_debug", 0);
$DEBUG = Irssi::settings_get_bool("bleh_debug");

Irssi::settings_add_bool("autobleh", "bleh_updates", 1);
check_updates();
